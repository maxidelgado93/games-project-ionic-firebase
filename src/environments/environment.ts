// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAWCnuriSZlZ98aqfABrXx1WG158WbEr-E",
    authDomain: "proyecto-videojuegos.firebaseapp.com",
    databaseURL: "https://proyecto-videojuegos.firebaseio.com",
    projectId: "proyecto-videojuegos",
    storageBucket: "proyecto-videojuegos.appspot.com",
    messagingSenderId: "608577442654",
    appId: "1:608577442654:web:fed2aae5fe894b6471f5cf"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
