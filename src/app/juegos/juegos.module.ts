//import { Juegos } from './juegos';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { JuegosComponent } from './juegos.component';


@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: JuegosComponent }])
  ],
  declarations: [JuegosComponent],
  exports: []
})
export class JuegosModule {}