import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service'
import { Juegos } from './juegos'
import { JuegosService } from '../services/juegos.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.scss'],
  providers: [ApiService]
})
export class JuegosComponent implements OnInit {

  juegos = [];
  searchTerm: string = "";
  searchededItems: Juegos[];
  // showing: string = "juegos";
  // selectedItems = [];
  // filteredItems = [];
  // status: string = 'juegos';

  public Juegos: any;

  constructor(
    private _apiService: ApiService,
    private juegosService: JuegosService
  ) {

  }

  ngOnInit() {
    this._apiService.getJuegos(this.searchTerm).subscribe(
      result => {
        this.juegos = result.results;
        /* this.genero = result.results; */
        console.log(this.juegos);
      },
      error => {
        console.log(<any>error)
      }
    )
  }

  setSearchedItems() {
    console.log("Searching term: ", this.searchTerm);
    /* this.searchededItems = this.searchItems(this.searchTerm); */
    this._apiService.getJuegos(this.searchTerm).subscribe(
      result => {
        this.searchededItems = result.results;
        /* this.genero = result.results; */
        console.log(this.juegos);
      },
      error => {
        console.log(<any>error)
      }
    )
  }
  searchItems(searchTerm) {
    return this.juegos.filter((item) => {
      let title = item.name;
      return title.toLowerCase().includes(searchTerm.toLowerCase());
    });
  }

  toggleFavorite(juego) {
    if(juego.favorite && juego.favorite == true) {
      this.juegosService.removeFavorite(juego);
    }
    else{
      this.juegosService.addFavorite(juego);
    }
  }


}
