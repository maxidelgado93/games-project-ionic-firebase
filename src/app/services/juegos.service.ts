import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JuegosService {

  selectedJuego;
  favoriteJuegos = [];

  constructor(){}

  setSelectedJuego(juego){
    this.selectedJuego = juego;
  }

  getSelectedJuego(){
    return this.selectedJuego;
  }

  addFavorite(juego) {
    this.favoriteJuegos.push(juego);
    juego.favorite = true;
    console.log("Los favoritos son: ", this.favoriteJuegos);
  }

  removeFavorite(juego) {
    juego.favorite = false;
    this.favoriteJuegos.map((currElement, index) => {
      if(currElement.id == juego.id){
        this.favoriteJuegos.splice(index,1);
      }
    });
    console.log("Los favoritos son: ", this.favoriteJuegos);
  }

  getFavorites() {
    return this.favoriteJuegos;
  }
}
