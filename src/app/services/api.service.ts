import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
/* import { Juegos } from '../juegos'; */


@Injectable({
    providedIn: 'root'
})

export class ApiService{
    public api: string
    constructor(
        public _http: HttpClient
    ){
        this.api = `https://api.rawg.io/api/games?page_size=40`;
    }
    getJuegos(term): Observable <any>{
        
        this.api = `https://api.rawg.io/api/games?page_size=40&search=${term}`;
        console.log('la busqueda actual es: ', this.api);
        return this._http.get(this.api);
    }
}